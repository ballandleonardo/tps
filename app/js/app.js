'use strict';

angular.module('tpsApp', [
  'ngCookies',
  'ngRoute',
  'ngAnimate',
  'ngMessages',
  'ngResource',
  'ngSanitize',
  'mm.foundation'
  ])

.config(function ($routeProvider, $locationProvider, $httpProvider) {    
    $routeProvider.
      when('/', {
        templateUrl: 'partials/main',
        controller: 'MainCtrl'
      }).
      when('/public', {
        templateUrl: 'partials/public',
        controller: 'PublicCtrl'
      }).
      when('/formations', {
        templateUrl: 'partials/formations',
        controller: 'FormationsCtrl'
      }).
      when('/profil', {
        templateUrl: 'partials/profil',
        controller: 'ProfilCtrl'
      }).
      when('/contact', {
        templateUrl: 'partials/contact',
        controller: 'ContactCtrl'
      }).
      otherwise({
        redirectTo:'/'
      });

    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('!');
  })
  
  .run(function ($rootScope, $location) {
        $rootScope.getClass = function(path) {
            if ($location.path().substr(0, path.length) == path) {
              return "active"
            } else {
              return ""
            }
        }
  });