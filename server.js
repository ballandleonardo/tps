'use strict';

// Server Setup ====================================================================
var express = require('express'),
    app = express(),
    config = require('./server/config/config');

// Express Configuration ===============================================================
var app = express();
require('./server/config/express')(app);
require('./server/routes/routes')(app);


// Launch server ====================================================================
app.listen(config.port, config.ip, function () {
  console.log('Client and Web API server launched on %s:%d', config.ip, config.port);
});

// Expose ==========================================================================
exports = module.exports = app;