# Un temps pour soi
## By Leonardo BALLAND

-------------------------------------
            INFORMATIONS
-------------------------------------
- Type: Business website
- HTTP Server: NodeJS
- WEB Client: AngularJS

-------------------------------------
            SCREENSHOTS
-------------------------------------

[Screenshot1](https://ibb.co/kvtzLF)

[Screenshot2](https://ibb.co/h4MVSv)

[Screenshot3](https://ibb.co/kT4jnv)

[Screenshot4](https://ibb.co/dfhtfF)

[Screenshot5](https://ibb.co/ei3G0F)

-------------------------------------
            INSTALLATION
-------------------------------------
```shell
~# git clone https://ballandleonardo@bitbucket.org/ballandleonardo/tps.git
~# cd tps && npm install
~# npm start
```