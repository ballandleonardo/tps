'use strict';

var index = require('./lib'),
    middleware = require('./middleware'),
    nodemailer = require('nodemailer'),
    sgTransport = require('nodemailer-sendgrid-transport');

var transporter = nodemailer.createTransport(sgTransport({
    service: 'SendGrid',
    auth: {
        api_user: 'digitalz',
        api_key: '00789321dz'
    }
}));


module.exports = function(app) {
    
// Nodemailer
    app.post('/postEmail', function(req, res){
        var mailOptions = {
            from: req.body.name + req.body.email,
            to: 'untempspoursoi26@live.fr',
            subject: "Contact de " + req.body.name,
            text: 'Nom : ' + req.body.name + "Email : " + req.body.email + "Société : " + req.body.entreprise + "Téléphone : " + req.body.phone + "Message : " + req.body.message
        };
        transporter.sendMail(mailOptions, function(error, info){
            if(error){
                console.log(error);
            }else{
                console.log('Message sent: ' + info.response);
                res.send(200);
            }
        });
    });

  // All routes to use Angular routing in app/scripts/app.js
  app.route('/partials/*')
    .get(index.partials);
  app.route('/*')
    .get( middleware.setUserCookie, index.index);
};