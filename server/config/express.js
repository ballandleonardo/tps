'use strict';

var express = require('express'),
    favicon = require('serve-favicon'),
    logger = require('morgan'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    cookieParser = require('cookie-parser'),
    path = require('path'),
    http = require('http'),
    cors = require('cors'),
    config = require('./config');

/**
 * Express configuration
 */
module.exports = function(app) {
    app.use(cors());
    app.use(require('prerender-node').set('prerenderToken', 'HcRXuPzPc0wttPUNedzW'));
    app.use(favicon(config.root + '/app/img/favicon.ico'));
    app.use(express.static(path.join(config.root, '.tmp')));
    app.use(express.static(path.join(config.root, 'app')));
    app.set('views', config.root + '/app/views');
    app.set('view engine', 'jade');
    app.use(bodyParser()); // get information from html forms
    app.use(cookieParser()); // read cookies (needed for auth)
    app.use(methodOverride());
    app.use(logger('dev')); // log every request to the console
};